package it.temera.devicemanager.springdevicemanager.fsm;

public interface FSMMessage<T extends FSMMessageType> {
    T getType();
}
