package it.temera.devicemanager.springdevicemanager.components.drivers.commons.events;

import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidReaderDto;

public class RfidReaderStartEvent<R extends RfidReaderDto> extends RfidReaderEvent<R> {
    public RfidReaderStartEvent(R reader) {
        super(reader);
    }
}
