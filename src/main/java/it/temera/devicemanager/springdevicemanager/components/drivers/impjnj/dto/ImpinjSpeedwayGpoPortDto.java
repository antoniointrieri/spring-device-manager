package it.temera.devicemanager.springdevicemanager.components.drivers.impjnj.dto;


import it.temera.devicemanager.springdevicemanager.components.drivers.impjnj.enums.ImpinjSpeedwayGpoMode;

public class ImpinjSpeedwayGpoPortDto {

    private int portNumber;

    private ImpinjSpeedwayGpoMode gpoMode;

    private long pulseDuration;

    public int getPortNumber() {
        return portNumber;
    }

    public void setPortNumber(int portNumber) {
        this.portNumber = portNumber;
    }

    public ImpinjSpeedwayGpoMode getGpoMode() {
        return gpoMode;
    }

    public void setGpoMode(ImpinjSpeedwayGpoMode gpoMode) {
        this.gpoMode = gpoMode;
    }

    public long getPulseDuration() {
        return pulseDuration;
    }

    public void setPulseDuration(long pulseDuration) {
        this.pulseDuration = pulseDuration;
    }

    public static ImpinjSpeedwayGpoPortDto createDefaultForPort(int portNumber) {
        ImpinjSpeedwayGpoPortDto res = new ImpinjSpeedwayGpoPortDto();
        res.setPortNumber(portNumber);
        res.setGpoMode(ImpinjSpeedwayGpoMode.NORMAL);
        res.setPulseDuration(0);
        return res;
    }
}
