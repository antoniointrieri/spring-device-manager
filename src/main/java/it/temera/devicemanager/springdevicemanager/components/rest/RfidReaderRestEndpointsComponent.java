package it.temera.devicemanager.springdevicemanager.components.rest;

import it.temera.devicemanager.springdevicemanager.DMComponent;
import it.temera.devicemanager.springdevicemanager.components.drivers.ReaderManagerComponent;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidReaderDto;

//componente per esporre gli endpoint di controllo di un lettore
public class RfidReaderRestEndpointsComponent extends DMComponent {

    public RfidReaderRestEndpointsComponent(RestServerComponent restServer, ReaderManagerComponent readerManager) {
        restServer.createPost("/rfidAntennas/:antennaId/start", ctx -> {
            RfidReaderDto reader = ctx.bodyAsClass(RfidReaderDto.class);
            readerManager.connect(reader);
            readerManager.start(reader);
            ctx.result(ctx.body());
        });


        restServer.createPost("/rfidAntennas/:antennaId/stop", ctx -> {
            RfidReaderDto reader = ctx.bodyAsClass(RfidReaderDto.class);
            readerManager.stop(reader);
            readerManager.disconnect(reader);

            ctx.result(ctx.body());
        });
    }
}
