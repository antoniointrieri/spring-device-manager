package it.temera.devicemanager.springdevicemanager.components.mhart.fsm;

public enum MhartDriverState {
    IDLE,
    READY,
    ACTIVE,
    AWAIT_DESTINATION,
    ERROR;
}
