package it.temera.devicemanager.springdevicemanager.components.mhart.dto;

import it.temera.devicemanager.springdevicemanager.components.mhart.enums.PLCMessageType;
import it.temera.devicemanager.springdevicemanager.components.mhart.fsm.MhartMessage;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class PLCCommand {

    private PLCMessageType messageType;
    private String plcIdentifier;
    private String payload;
    private String raw;

    public static PLCCommand fromString(String command) {
        PLCCommand cmd = null;
        PLCMessageType plcMessageType = PLCMessageType.fromCommand(command);
        if (plcMessageType != null) {
            cmd = new PLCCommand();
            cmd.setRaw(command);
            cmd.setMessageType(plcMessageType);
            int plcIdLength = plcMessageType.getMessageTypeLength() + plcMessageType.getPlcIdentifierLength();
            cmd.setPlcIdentifier(command.substring(plcMessageType.getMessageTypeLength(), plcIdLength));
            cmd.setPayload(command.substring(plcIdLength));
        }
        return cmd;
    }

    public static PLCCommand fromMessage(MhartMessage message) {
        return fromString(message.getParams());
    }
}
