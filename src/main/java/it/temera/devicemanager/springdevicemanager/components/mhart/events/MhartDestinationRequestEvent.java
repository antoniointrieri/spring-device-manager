package it.temera.devicemanager.springdevicemanager.components.mhart.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class MhartDestinationRequestEvent {
    private final String lastDestinationRequestID;
}
