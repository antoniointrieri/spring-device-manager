package it.temera.devicemanager.springdevicemanager.components.drivers.impjnj;

import com.impinj.octane.*;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidResponseDto;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.concurrent.CompletableFuture;

class ImpinjSpeedwayOctaneOperationListener implements TagOpCompleteListener {

    private final Logger logger = Logger.getLogger(ImpinjSpeedwayOctaneOperationListener.class);
    private final CompletableFuture<RfidResponseDto> future;
    private boolean success = false;

    public ImpinjSpeedwayOctaneOperationListener(CompletableFuture<RfidResponseDto> future) {
        this.future = future;
    }

    @Override
    public void onTagOpComplete(ImpinjReader reader, TagOpReport results) {
        logger.info("TagOpComplete: ");
        String message = "";
        for (TagOpResult t : results.getResults()) {
            logger.info("  EPC: " + t.getTag().getEpc().toHexString());
            if (t instanceof TagWriteOpResult) {
                TagWriteOpResult tr = (TagWriteOpResult) t;

                if (tr.getResult().equals(WriteResultStatus.Success)) {
                    success = true;
                }
                message = tr.getResult().toString();
            } else if (t instanceof TagReadOpResult) {
                TagReadOpResult tr = (TagReadOpResult) t;

                if (tr.getResult().equals(ReadResultStatus.Success)) {
                    success = true;
                }
                message = StringUtils.deleteWhitespace(tr.getData().toString());
            }
        }
        future.complete(new RfidResponseDto(success, message));
    }

}