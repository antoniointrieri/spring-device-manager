package it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto;

/**
 * @author Matteo Lapini
 * @since 4.8
 */
public class UhfScanningDto extends ScanningDto {
    private String epc;
    private String tid;
    private String memory;
    private int channelIndex;
    private long firstSeen;
    private int inventoryParameterSpecID;
    private long lastSeen;
    private double peakRSSI;
    private int roSpecID;
    private int tagSeenCount;
    private double phaseAngleInRadians;
    private double rfDopplerFrequency;

    public double getPhaseAngleInRadians() {
        return phaseAngleInRadians;
    }

    public void setPhaseAngleInRadians(double phaseAngleInRadians) {
        this.phaseAngleInRadians = phaseAngleInRadians;
    }

    public double getRfDopplerFrequency() {
        return rfDopplerFrequency;
    }

    public void setRfDopplerFrequency(double rfDopplerFrequency) {
        this.rfDopplerFrequency = rfDopplerFrequency;
    }

    public String getEpc() {
        return epc;
    }

    public String getTid() {
        return tid;
    }

    public String getMemory() {
        return memory;
    }

    public int getChannelIndex() {
        return channelIndex;
    }

    public long getFirstSeen() {
        return firstSeen;
    }

    public int getInventoryParameterSpecID() {
        return inventoryParameterSpecID;
    }

    public long getLastSeen() {
        return lastSeen;
    }

    public double getPeakRSSI() {
        return peakRSSI;
    }

    public int getRoSpecID() {
        return roSpecID;
    }

    public int getTagSeenCount() {
        return tagSeenCount;
    }

    @Override
    public String getCode() {
        return epc;
    }

    public void setEpc(String epc) {
        this.epc = epc;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public void setMemory(String memory) {
        this.memory = memory;
    }

    public void setChannelIndex(int channelIndex) {
        this.channelIndex = channelIndex;
    }

    public void setFirstSeen(long firstSeen) {
        this.firstSeen = firstSeen;
    }

    public void setInventoryParameterSpecID(int inventoryParameterSpecID) {
        this.inventoryParameterSpecID = inventoryParameterSpecID;
    }

    public void setLastSeen(long lastSeen) {
        this.lastSeen = lastSeen;
    }

    public void setPeakRSSI(double peakRSSI) {
        this.peakRSSI = peakRSSI;
    }

    public void setRoSpecID(int roSpecID) {
        this.roSpecID = roSpecID;
    }

    public void setTagSeenCount(int tagSeenCount) {
        this.tagSeenCount = tagSeenCount;
    }
}
