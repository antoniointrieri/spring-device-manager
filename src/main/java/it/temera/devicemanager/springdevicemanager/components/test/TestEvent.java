package it.temera.devicemanager.springdevicemanager.components.test;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class TestEvent {
    private final String body;
}
