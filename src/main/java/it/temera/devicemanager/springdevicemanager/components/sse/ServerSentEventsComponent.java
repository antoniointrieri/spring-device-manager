package it.temera.devicemanager.springdevicemanager.components.sse;

import io.javalin.Javalin;
import io.javalin.http.sse.SseClient;
import it.temera.devicemanager.springdevicemanager.DMComponent;

import java.util.concurrent.ConcurrentLinkedQueue;

public class ServerSentEventsComponent extends DMComponent {
    private final Javalin javalin;
    private final ConcurrentLinkedQueue<SseClient> clients = new ConcurrentLinkedQueue<>();

    public ServerSentEventsComponent(Integer port) {
        javalin = Javalin.create(config -> {
            config.showJavalinBanner = false;
        });
        var thread = new Thread(() -> javalin.start(port));
        thread.start();
    }

    @Override
    public void destroy() {
        javalin.stop();
    }

    public void addSSE(String endpoint) {
        javalin.sse(endpoint, client -> {
            clients.add(client);
            client.onClose(() -> {
                clients.remove(client);
            });
        });
    }

    public void send(String body) {
        clients.forEach(client -> {
            client.sendEvent(body);
        });
    }
}
