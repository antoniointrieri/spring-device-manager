package it.temera.devicemanager.springdevicemanager.components.mhart.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class MhartClearEvent {
    private final String lastDestinationRequestId;
}
