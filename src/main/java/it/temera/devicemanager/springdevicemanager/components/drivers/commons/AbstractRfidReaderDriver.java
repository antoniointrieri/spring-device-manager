package it.temera.devicemanager.springdevicemanager.components.drivers.commons;


import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidCommandDto;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidReaderDto;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidResponseDto;
import org.apache.commons.lang3.StringUtils;

import java.time.Instant;
import java.util.concurrent.CompletableFuture;


public abstract class AbstractRfidReaderDriver<T extends RfidReaderDto> {

    protected long lastKeepalive;
    protected boolean started;
    protected T reader = null;

    public abstract void connect(T params);

    public abstract void disconnect();

    public abstract CompletableFuture<RfidResponseDto> start(T params);

    public abstract CompletableFuture<RfidResponseDto> stop();

    public abstract CompletableFuture<RfidResponseDto> write(RfidCommandDto dto);

    public abstract CompletableFuture<RfidResponseDto> read(RfidCommandDto dto);

    public abstract CompletableFuture<RfidResponseDto> kill(RfidCommandDto dto);


    protected synchronized void updateKeepalive() {
        lastKeepalive = Instant.now().toEpochMilli();
    }

    protected synchronized boolean isKeepAliveEnabled() {
        return false;
    }

    protected long getLastKeepaliveTime() {
        return lastKeepalive;
    }

    protected long getKeepAlivePeriod() {
        return 10000;
    }

    protected boolean isStarted() {
        return false;
    }

    protected void setStarted(boolean started) {
        this.started = started;
    }

    protected String normalizeEpc(String epc) {
        return StringUtils.leftPad(StringUtils.deleteWhitespace(epc),
                Integer.parseInt(System.getProperty("tmr.kernel.epc.length", "24")), "0");
    }

    protected CompletableFuture<RfidResponseDto> success() {
        return CompletableFuture.completedFuture(new RfidResponseDto(true));
    }
}
