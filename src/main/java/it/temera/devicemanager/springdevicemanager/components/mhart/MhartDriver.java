package it.temera.devicemanager.springdevicemanager.components.mhart;

import it.temera.devicemanager.springdevicemanager.DMComponent;
import it.temera.devicemanager.springdevicemanager.components.mhart.dto.PLCCommand;
import it.temera.devicemanager.springdevicemanager.components.mhart.events.MhartClearEvent;
import it.temera.devicemanager.springdevicemanager.components.mhart.events.MhartDestinationRequestEvent;
import it.temera.devicemanager.springdevicemanager.components.mhart.exceptions.MhartDriverException;
import it.temera.devicemanager.springdevicemanager.components.mhart.fsm.MhartDriverState;
import it.temera.devicemanager.springdevicemanager.components.mhart.fsm.MhartMessage;
import it.temera.devicemanager.springdevicemanager.components.mhart.fsm.MhartMessageType;
import it.temera.devicemanager.springdevicemanager.components.server.TcpServer;
import it.temera.devicemanager.springdevicemanager.components.server.events.TcpServerEvent;
import it.temera.devicemanager.springdevicemanager.fsm.FSM;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.event.EventListener;

import java.util.List;

import static it.temera.devicemanager.springdevicemanager.components.mhart.fsm.MhartDriverState.*;
import static it.temera.devicemanager.springdevicemanager.components.mhart.fsm.MhartMessageType.*;

@Slf4j
public class MhartDriver extends DMComponent {
    private final List<String> ALLOWED_DESTINATIONS = List.of("00001",    //OK
            "00002",    //CQ
            "00099");   //NOT OK
    private FSM<MhartDriverState, MhartMessageType, MhartMessage> stateMachine;
    private String plcID;
    private TcpServer tcpServerComponent;
    private String lastDestinationRequestID;

    public MhartDriver(TcpServer server) {
        this.tcpServerComponent = server;

        stateMachine = FSM.<MhartDriverState, MhartMessageType, MhartMessage>builder().transition(IDLE, READY,
                PLC_READY, this::onPlcReady)
                .transition(List.of(READY, ERROR), ACTIVE, CLIENT_READY, this::onClientReady)
                .transition(ACTIVE, AWAIT_DESTINATION, PLC_DESTINATION_REQUEST, this::onDestinationRequested)

                //Il client mi manda la risposta della destinazione
                .transition(AWAIT_DESTINATION, ACTIVE, CLIENT_DESTINATION_RESPONSE, this::onDestinationReceived)

                //Il plc va in timeout e mi manda un clear
                .transition(AWAIT_DESTINATION, ACTIVE, PLC_CLEAR, this::onClear)

                //Se il client manda un alarm inoltra l'alarm al PLC
                .transition(List.of(ACTIVE, AWAIT_DESTINATION), ERROR, CLIENT_ALARM, this::onClientAlarm)

                //
                .transition(List.of(ACTIVE, ERROR), IDLE, CLIENT_RESET) //da capire se questa transizione serve o meno
                .build(IDLE);
    }

    //il client mi dice che è pronto
    public void clientReady() {
        manageMessage(new MhartMessage(CLIENT_READY));
    }

    //il client manda un alarm
    public void clientAlarm(String body) {
        manageMessage(new MhartMessage(CLIENT_ALARM, body));
    }

    //risposta a una richiesta di destinazione
    public void clientDestinationResponse(String destination) {
        manageMessage(new MhartMessage(CLIENT_DESTINATION_RESPONSE, destination));
    }

    public MhartDriverState getState() {
        return stateMachine.getState();
    }

    //il client vuole resettare lo stato del conveyor
    public void reset() {
        manageMessage(new MhartMessage(CLIENT_RESET));
    }

    protected void manageMessage(MhartMessage message) {
        //Tratta in maniera uniforme messaggi provenienti dal client o dal plc
        log.info("Received message " + message.getType().name() + " with body " + message.getParams());
        try {
            var success = stateMachine.next(message);
            if (!success) {
                //TODO: gestisci il caso in cui sia fallito un cambio di stato
            }
        } catch (MhartDriverException e) {
            //TODO: manda allarme
        } catch (Exception e) {

        }
    }

    @EventListener
    private void onReceiveMessageFromPLC(TcpServerEvent event) {
        String body = event.getBody();
        String prefix = body.substring(0, 2);
        MhartMessageType command;
        switch (prefix) {
            case "RY":
                command = PLC_READY;
                break;
            case "RD":
                command = PLC_DESTINATION_REQUEST;
                break;
            case "TO":
                command = PLC_CLEAR;
                break;
            default:
                throw new RuntimeException("Unknown command prefix " + prefix);
        }
        manageMessage(new MhartMessage(command, body));
    }

    protected void onPlcReady(MhartMessage message) {
        //Attendi che anche il client sia pronto
        //Il messaggio è della forma RYPLC01READY
        plcID = PLCCommand.fromMessage(message).getPlcIdentifier();
    }

    protected void onClientReady(MhartMessage message) {
        //Invia al PLC il messaggio di ready
        sendToPLC("RY" + plcID + "READY");
    }

    protected void onDestinationRequested(MhartMessage message) {
        //Attendi che il client invii la destinazione
        //Il messaggio è della forma RDPLC0100001
        lastDestinationRequestID = PLCCommand.fromMessage(message).getPayload();
        fire(new MhartDestinationRequestEvent(lastDestinationRequestID));
    }

    protected void onDestinationReceived(MhartMessage message) {
        //Ho ricevuto la destinazione da parte del client, inoltrala al PLC
        String destination = message.getParams();

        if (!ALLOWED_DESTINATIONS.contains(destination)) {
            throw new RuntimeException("Invalid destination " + destination);
        }
        sendToPLC("RD" + plcID + lastDestinationRequestID + destination);
    }

    protected void onClear(MhartMessage mhartMessage) {
        //Notifica al client che il PLC è andato in timeout
        fire(new MhartClearEvent(lastDestinationRequestID));
    }

    protected void onClientAlarm(MhartMessage message) {
        //Il client mi ha mandato un Alarm, propagalo al PLC
        sendAlarm(message.getParams());
    }

    protected void sendAlarm(String alarmCode) {
        //Es: ALPLC01000000000000ANTENNA1
        sendToPLC("AL" + plcID + StringUtils.leftPad(alarmCode, 20, "0").substring(0, 20));
    }

    private void sendToPLC(String message) {
        tcpServerComponent.send(message);
    }

    @Override
    public void destroy() {
        tcpServerComponent.destroy();
    }
}
