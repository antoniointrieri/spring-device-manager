package it.temera.devicemanager.springdevicemanager.components.mhart.enums;

import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

public enum PLCMessageType {
    // AABBBBBCCCCC
    READY("RY", 2, 5, 5), //
    // AABBBBBDDDDDDDDDDDDDDDDDDDD
    ALARM("AL", 2, 5, 20), //
    // AABBBBBCCCCC
    DESTINATION("RD", 2, 5, 5), //
    // AABBBBBCCCCCCDDDDD
    CLEAR("TO", 2, 5, 11);

    @Getter
    private final String messageType;
    @Getter
    private final int messageTypeLength;
    @Getter
    private final int plcIdentifierLength;
    @Getter
    private final int payloadLength;

    PLCMessageType(String messageType, int messageTypeLength, int plcIdentifierLength, int payloadLength) {
        this.messageType = messageType;
        this.messageTypeLength = messageTypeLength;
        this.plcIdentifierLength = plcIdentifierLength;
        this.payloadLength = payloadLength;
    }

    public static PLCMessageType fromCommand(String command) {
        if (StringUtils.isNotBlank(command)) {
            for (PLCMessageType mt : PLCMessageType.values()) {
                String messType = command.substring(0, mt.getMessageTypeLength());
                if (messType.equalsIgnoreCase(mt.getMessageType())) {
                    return mt;
                }
            }
        }
        return null;
    }
}
