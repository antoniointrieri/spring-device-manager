package it.temera.devicemanager.springdevicemanager.fsm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Implementazione basilare di una macchina a stati finiti (Finite State Machine).
 * Permette di gestire in maniera sintetica i cambiamenti di stato, definendo le transizioni ammesse e i messaggi
 * che portano a tali transizioni. In corrispondenza di ogni cambio di stato è possibile eseguire un'azione.
 * Per la costruzione di una nuova FSM utilizzare il builder {@link Builder}.
 * @param <S> La classe che identifica lo stato. Non ci sono vincoli di tipo su questa classe.
 * @param <T> La classe del tipo messaggi. Sulla base del tipo dei messaggi viene determinata la possibilità effettuare
 *           un cambio di stato
 * @param <M> La classe dei messaggi. I messaggi vengono passati alle azioni opzionali effettuate in corrispondenza dei
 *           passaggi di stato
 */
public class FSM<S extends Enum<S>, T extends FSMMessageType, M extends FSMMessage<T>> {
    private final List<FSMTransition<S, T, M>> transitions;
    private S state;

    /**
     * Elabora il messaggio fornito e valuta se dallo stato attuale è possibile passare a un nuovo stato.
     * @param message Il messaggio utilizzato per valutare il cambio di stato
     * @return true se il cambio di stato è stato effettuato, false altrimenti
     */
    public Boolean next(M message) {
        Optional<FSMTransition<S, T, M>> transition = transitions.stream()
                .filter(it -> (it.getFrom() == null || it.getFrom().contains(state)) && it.getMessageType().equals(message.getType()))
                .findFirst();
        transition.ifPresent(it -> {
            state = it.getTo();
            if (it.getAction() != null) {
                it.getAction().accept(message);
            }
        });
        return transition.isPresent();
    }


    public static class Builder<S extends Enum<S>, T extends FSMMessageType, M extends FSMMessage<T>> {
        private final List<FSMTransition<S, T, M>> transitions = new ArrayList<>();

        private Builder() {
        }

        /**
         * Definisce una transizione di stato da una lista di stati {@code from} a {@code to} quando viene ricevuto un
         * messaggio di tipo {@code messageType}. Inoltre indica che subito dopo il cambio di stato viene invocata la
         * funzione {@code action} con parametro il messaggio che ha determinato il cambio di stato.
         * Se {@code from} è null, allora consenti la transizione da qualunque stato di partenza.
         * NB: non viene fatto nessun controllo sulla presenza di transizioni duplicate o con condizioni di transizioni
         * identiche. Sta all'utente assicurarsi che non ci siano errori nella costruzione della FSM.
         * @param from Lista di stati di partenza. Se null consenti la transizione da qualunque stato
         * @param to Lo stato di arrivo
         * @param messageType Il messaggio che permette tale cambio di stato
         * @param action L'azione da effettuare in corrispondenza del passaggio di stato
         * @return il builder stesso
         */
        public Builder<S, T, M> transition(List<S> from, S to, T messageType, Consumer<M> action) {
            transitions.add(new FSMTransition<>(from, to, messageType, action));
            return this;
        }

        /**
         * Definisce una transizione di stato da uno stato {@code from} a {@code to} quando viene ricevuto un
         * messaggio di tipo {@code messageType}. Inoltre indica che subito dopo il cambio di stato viene invocata la
         * funzione {@code action} con parametro il messaggio che ha determinato il cambio di stato.
         * Se {@code from} è null, allora consenti la transizione da qualunque stato di partenza.
         * NB: non viene fatto nessun controllo sulla presenza di transizioni duplicate o con condizioni di transizioni
         * identiche. Sta all'utente assicurarsi che non ci siano errori nella costruzione della FSM.
         * @param from Lo stato di partenza. Se null consenti la transizione da qualunque stato
         * @param to Lo stato di arrivo
         * @param messageType Il messaggio che permette tale cambio di stato
         * @param action L'azione da effettuare in corrispondenza del passaggio di stato
         * @return il builder stesso
         */
        public Builder<S, T, M> transition(S from, S to, T messageType, Consumer<M> action) {
            transition(Collections.singletonList(from), to, messageType, action);
            return this;
        }

        /**
         * Definisce una transizione di stato da una lista di stati {@code from} a {@code to} quando viene ricevuto un
         * messaggio di tipo {@code messageType}. Invocare questa funzione equivale ad invocare
         * {@link Builder#transition(List, Enum, FSMMessageType, Consumer)} fornendo null come ultimo parametro
         * Se {@code from} è null, allora consenti la transizione da qualunque stato di partenza.
         * @param from La lista di stati di partenza. Se null consenti la transizione da qualunque stato
         * @param to Lo stato di arrivo
         * @param messageType Il messaggio che permette tale cambio di stato
         * @return il builder stesso
         */
        public Builder<S, T, M> transition(List<S> from, S to, T messageType) {
            transition(from, to, messageType, null);
            return this;
        }

        /**
         * Definisce una transizione di stato da uno stato {@code from} a {@code to} quando viene ricevuto un
         * messaggio di tipo {@code messageType}. Invocare questa funzione equivale ad invocare
         * {@link Builder#transition(List, Enum, FSMMessageType, Consumer)} fornendo null come ultimo parametro
         * Se {@code from} è null, allora consenti la transizione da qualunque stato di partenza.
         * @param from Lo stato di partenza. Se null consenti la transizione da qualunque stato
         * @param to Lo stato di arrivo
         * @param messageType Il messaggio che permette tale cambio di stato
         * @return il builder stesso
         */
        public Builder<S, T, M> transition(S from, S to, T messageType) {
            transition(Collections.singletonList(from), to, messageType, null);
            return this;
        }

        /**
         * Costruisce la FSM.
         * @param initialState Lo stato iniziale della FSM.
         * @return La FSM.
         */
        public FSM<S, T, M> build(S initialState) {
            return new FSM<S, T, M>(transitions, initialState);
        }
    }

    /**
     * Crea una nuova istanza del builder dell'FSM.
     * Aggiungere transizioni con i metodi {@link Builder#transition(List, Enum, FSMMessageType)} o
     * {@link Builder#transition(List, Enum, FSMMessageType, Consumer)} e completare la costruzione chiamando
     * il metodo {@link Builder#build(Enum)}
     */
    public static <S extends Enum<S>, T extends FSMMessageType, M extends FSMMessage<T>> Builder<S, T, M> builder() {
        return new Builder<>();
    }

    private FSM(List<FSMTransition<S, T, M>> transitions, S initialState) {
        state = initialState;
        this.transitions = transitions;
    }

    public S getState() {
        return state;
    }
}
