package it.temera.devicemanager.springdevicemanager.components.server.events;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class TcpServerEvent {
    private final String body;
}
