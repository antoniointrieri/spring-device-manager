package it.temera.devicemanager.springdevicemanager.components.drivers.commons.exceptions;

public class RfidReaderException extends RuntimeException {

    private static final long serialVersionUID = 7443717811199715402L;

    public RfidReaderException() {
        super();
    }

    public RfidReaderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public RfidReaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public RfidReaderException(String message) {
        super(message);
    }

    public RfidReaderException(Throwable cause) {
        super(cause);
    }

}
