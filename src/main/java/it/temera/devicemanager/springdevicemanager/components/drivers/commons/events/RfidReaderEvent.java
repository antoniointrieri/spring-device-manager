package it.temera.devicemanager.springdevicemanager.components.drivers.commons.events;

import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidReaderDto;
import lombok.Getter;

@Getter
public class RfidReaderEvent<R extends RfidReaderDto> {
    private final R reader;
    public RfidReaderEvent(R reader) {
        this.reader = reader;
    }
}
