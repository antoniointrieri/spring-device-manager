package it.temera.devicemanager.springdevicemanager.components.server;

import it.temera.devicemanager.springdevicemanager.DMComponent;
import it.temera.devicemanager.springdevicemanager.components.server.events.TcpServerEvent;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

@Slf4j
public class TcpServer extends DMComponent {
    private ServerSocket server;
    private Socket client;
    private BufferedReader input;
    private PrintWriter output;

    private boolean killed = false;

    public TcpServer(Integer port) {
        try {
            server = new ServerSocket(port);
            Thread thread = new Thread(() -> {
                try {
                    client = server.accept();
                    log.info("Client accepted");
                    input = new BufferedReader(new InputStreamReader(client.getInputStream()));
                    output = new PrintWriter(client.getOutputStream(), true);
                    log.info("Channel up");
                    while (!killed) {
                        String message;
                        while (input.ready() && (message = input.readLine()) != null) {
                            log.info("Message received: {}", message);
                            fire(new TcpServerEvent(message));
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    output.close();
                    try {
                        input.close();
                        client.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void send(String body) {
        output.println(body);
    }

    @Override
    public void destroy() {
        try {
            server.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        killed = true;
    }


}
