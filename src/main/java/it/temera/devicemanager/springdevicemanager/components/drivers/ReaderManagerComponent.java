package it.temera.devicemanager.springdevicemanager.components.drivers;

import it.temera.devicemanager.springdevicemanager.DMComponent;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.AbstractRfidReaderDriver;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidReaderDto;
import it.temera.devicemanager.springdevicemanager.components.drivers.impjnj.ImpinjSpeedwayOctaneDriver;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ReaderManagerComponent extends DMComponent {
    private final Map<String, AbstractRfidReaderDriver<?>> readers = new ConcurrentHashMap<>();

    public <T extends RfidReaderDto> void connect(T dto) {
        AbstractRfidReaderDriver reader;
        if (!readers.containsKey(dto.getId())) {
            //crea il reader corretto
            reader = new ImpinjSpeedwayOctaneDriver(this);
            readers.put(dto.getId(), reader);
        }
        reader = readers.get(dto.getId());
        reader.connect(dto);
    }

    public <T extends RfidReaderDto> void start(T dto) {
        AbstractRfidReaderDriver reader = getReader(dto.getId());
        reader.start(dto);
    }

    public <T extends RfidReaderDto> void stop(T dto) {
        var reader = getReader(dto.getId());
        reader.stop();
    }

    public <T extends RfidReaderDto> void disconnect(T dto) {
        var reader = getReader(dto.getId());
        reader.disconnect();
    }

    private AbstractRfidReaderDriver<?> getReader(String id) {
        if (readers.containsKey(id)) {
            return readers.get(id);
        } else {
            throw new RuntimeException("Reader not found");
        }
    }

}
