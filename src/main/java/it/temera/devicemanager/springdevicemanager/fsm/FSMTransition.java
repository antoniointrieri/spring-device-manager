package it.temera.devicemanager.springdevicemanager.fsm;

import lombok.Getter;

import java.util.List;
import java.util.function.Consumer;

@Getter
public class FSMTransition<S, T extends FSMMessageType, M extends FSMMessage<T>> {
    private final List<S> from;
    private final S to;
    private final T messageType;
    private final Consumer<M> action;

    public FSMTransition(List<S> from, S to, T messageType, Consumer<M> action) {
        this.from = from;
        this.to = to;
        this.messageType = messageType;
        this.action = action;
    }

}
