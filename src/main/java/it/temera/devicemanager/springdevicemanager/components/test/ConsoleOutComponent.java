package it.temera.devicemanager.springdevicemanager.components.test;

import it.temera.devicemanager.springdevicemanager.DMComponent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ConsoleOutComponent extends DMComponent {

    public ConsoleOutComponent() {
        log.info("Creating console out component");
    }

    @EventListener
    public void onTestEvent(TestEvent event) {
        log.info("Message is " + event.getBody());
    }

}
