package it.temera.devicemanager.springdevicemanager.components.websocket;

import io.javalin.Javalin;
import io.javalin.websocket.WsHandler;
import it.temera.devicemanager.springdevicemanager.DMComponent;

import java.util.function.Consumer;

public class WebSocketServerComponent extends DMComponent {
    private final Javalin javalin;

    public WebSocketServerComponent(Integer port) {
        javalin = Javalin.create(config -> {
            config.showJavalinBanner = false;
        });
        var thread = new Thread(() -> javalin.start(port));
        thread.start();
    }


    public void createChannel(String endpoint, Consumer<WsHandler> handler) {
        javalin.ws(endpoint, handler);
    }


    @Override
    public void destroy() {
        javalin.stop();
    }


}
