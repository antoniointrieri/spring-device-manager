package it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto;

import it.temera.devicemanager.springdevicemanager.dto.ResponseDto;

public class RfidResponseDto extends ResponseDto {
    private String epc;

    public RfidResponseDto(boolean success) {
        super(success);
    }

    public RfidResponseDto(boolean success, String epc) {
        super(success);
        this.epc = epc;
    }

    public String getEpc() {
        return epc;
    }
}
