package it.temera.devicemanager.springdevicemanager.components.rest;

import io.javalin.Javalin;
import io.javalin.http.Handler;
import it.temera.devicemanager.springdevicemanager.DMComponent;

public class RestServerComponent extends DMComponent {
    private final Javalin javalin;

    public RestServerComponent(Integer port) {
        javalin = Javalin.create(config -> {
            config.showJavalinBanner = false;
        });
        var thread = new Thread(() -> javalin.start(port));
        thread.start();
    }

    @Override
    public void destroy() {
        javalin.stop();
    }

    public void createPost(String endpoint, Handler handler) {
        javalin.post(endpoint, handler);
    }

    public void createGet(String endpoint, Handler handler) {
        javalin.get(endpoint, handler);
    }

}
