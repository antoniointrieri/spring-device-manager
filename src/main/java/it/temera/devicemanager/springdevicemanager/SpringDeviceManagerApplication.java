package it.temera.devicemanager.springdevicemanager;

import it.temera.devicemanager.springdevicemanager.components.drivers.ReaderManagerComponent;
import it.temera.devicemanager.springdevicemanager.components.drivers.impjnj.dto.ImpinjSpeedwayOctaneDto;
import it.temera.devicemanager.springdevicemanager.components.rest.RestServerComponent;
import it.temera.devicemanager.springdevicemanager.components.rest.RfidReaderRestEndpointsComponent;
import it.temera.devicemanager.springdevicemanager.components.websocket.RfidScanningWebSocketEndpointsComponent;
import it.temera.devicemanager.springdevicemanager.components.websocket.WebSocketServerComponent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Slf4j
public class SpringDeviceManagerApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(SpringDeviceManagerApplication.class, args);
    }


    @Bean
    public ReaderManagerComponent getReaderManager() {
        return new ReaderManagerComponent();
    }

    @Bean
    public WebSocketServerComponent getWS() {
        return new WebSocketServerComponent(8180);
    }

    @Bean
    public RfidScanningWebSocketEndpointsComponent getEndpoints() {
        return new RfidScanningWebSocketEndpointsComponent(getWS());
    }

    @Bean
    public RestServerComponent getRestServerComponent() {
        return new RestServerComponent(9181);
    }

    @Bean
    public RfidReaderRestEndpointsComponent getRfidReaderRestEndpointsComponent() {
        return new RfidReaderRestEndpointsComponent(getRestServerComponent(), getReaderManager());
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Running!");
    }
}
