package it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto;

import it.temera.devicemanager.springdevicemanager.components.drivers.commons.enums.MemoryBank;

public class RfidCommandDto {
    private String epc;
    private Long timeout;
    private MemoryBank memoryBank;
    private String data;
    private Integer address;
    private Integer numBlock;
    private String password;
    private Boolean beepOnSuccess;
    private Boolean beepOnError;

    public String getEpc() {
        return epc;
    }

    public void setEpc(String epc) {
        this.epc = epc;
    }

    public Long getTimeout() {
        return timeout;
    }

    public void setTimeout(Long timeout) {
        this.timeout = timeout;
    }

    public MemoryBank getMemoryBank() {
        return memoryBank;
    }

    public void setMemoryBank(MemoryBank memoryBank) {
        this.memoryBank = memoryBank;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Integer getAddress() {
        return address;
    }

    public void setAddress(Integer address) {
        this.address = address;
    }

    public Integer getNumBlock() {
        return numBlock;
    }

    public void setNumBlock(Integer numBlock) {
        this.numBlock = numBlock;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getBeepOnSuccess() {
        return beepOnSuccess;
    }

    public void setBeepOnSuccess(Boolean beepOnSuccess) {
        this.beepOnSuccess = beepOnSuccess;
    }

    public Boolean getBeepOnError() {
        return beepOnError;
    }

    public void setBeepOnError(Boolean beepOnError) {
        this.beepOnError = beepOnError;
    }
}
