package it.temera.devicemanager.springdevicemanager.components.drivers.commons.enums;

public enum MemoryBank {

    EPC, TID, USER, RESERVED;

}
