package it.temera.devicemanager.springdevicemanager.components.websocket;

import io.javalin.plugin.json.JavalinJackson;
import it.temera.devicemanager.springdevicemanager.DMComponent;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.events.TagReadEvent;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.jetty.websocket.api.Session;
import org.springframework.context.event.EventListener;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class RfidScanningWebSocketEndpointsComponent extends DMComponent {
    private final Map<String, Session> sessions = new HashMap<>();

    public RfidScanningWebSocketEndpointsComponent(WebSocketServerComponent wsServer) {
        wsServer.createChannel("/rfidAntennas/scannings", ws -> {
            ws.onConnect(ctx -> {
                var antennaId = ctx.queryParam("antennaId");
                var sessionId = ctx.queryParam("sessionId");
                log.info("Connected for " + antennaId + " " + sessionId);
                sessions.put(getSessionKey(antennaId), ctx.session);
            });

            ws.onClose(ctx -> {
                var antennaId = ctx.queryParam("antennaId");
                var sessionId = ctx.queryParam("sessionId");
                log.info("Disconnected for " + antennaId + " " + sessionId);
                sessions.remove(getSessionKey(antennaId));
            });
        });
    }

    @Override
    public void destroy() {
        sessions.forEach((key, value) -> value.close());
    }

    @EventListener
    private void onTagRead(TagReadEvent<?, ?> event) {
        log.info("Received event tag read "+event.getScanning().getCode());
        var dto = event.getScanning();
        String sessionKey = getSessionKey(dto.getAntennaIdDb());
        Session session = sessions.get(sessionKey);
        if (session == null) {
            log.trace("Unknown session with key " + sessionKey + ", no websocket client connected?");
            return;
        }
        try {
            session.getRemote().sendString(JavalinJackson.defaultObjectMapper().writeValueAsString(dto));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getSessionKey(String antennaId) {
        return antennaId;
    }
}
