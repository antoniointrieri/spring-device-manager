package it.temera.devicemanager.springdevicemanager.dto;

public class ResponseDto {
    private final boolean success;

    public ResponseDto(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }
}
