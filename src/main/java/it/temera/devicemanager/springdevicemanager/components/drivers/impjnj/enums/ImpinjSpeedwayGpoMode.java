package it.temera.devicemanager.springdevicemanager.components.drivers.impjnj.enums;

import com.impinj.octane.GpoMode;

public enum ImpinjSpeedwayGpoMode {

    //@formatter:off
    NORMAL(GpoMode.Normal),
    PULSED(GpoMode.Pulsed),
    READER_OPERATIONAL_STATUS(GpoMode.ReaderOperationalStatus),
    LLRP_CONNECTION_STATUS(GpoMode.LLRPConnectionStatus),
    READER_INVENTORY_STATUS(GpoMode.ReaderInventoryStatus),
    NETWORK_CONNECTION_STATUS(GpoMode.NetworkConnectionStatus),
    READER_INVENTORY_TAGS_STATUS(GpoMode.ReaderInventoryTagsStatus);
    //@formatter:on

    private GpoMode impinjGpoMode;

    ImpinjSpeedwayGpoMode(GpoMode impinjGpoMode) {
        this.impinjGpoMode = impinjGpoMode;
    }

    public GpoMode getImpinjGpoMode() {
        return impinjGpoMode;
    }
}
