package it.temera.devicemanager.springdevicemanager;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Slf4j
public abstract class DMComponent {
    @Autowired
    protected ApplicationEventPublisher publisher;

    public void fire(Object event) {
        log.info("Firing event "+event);
        publisher.publishEvent(event);
    }

    public void destroy() {

    }
}
