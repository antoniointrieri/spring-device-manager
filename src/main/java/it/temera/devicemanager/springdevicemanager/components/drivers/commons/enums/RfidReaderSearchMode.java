package it.temera.devicemanager.springdevicemanager.components.drivers.commons.enums;

/**
 * @author Dario Veneziani
 * @since 4.0
 *
 */
public enum RfidReaderSearchMode {

    DUAL_TARGET, SINGLE_TARGET

}
