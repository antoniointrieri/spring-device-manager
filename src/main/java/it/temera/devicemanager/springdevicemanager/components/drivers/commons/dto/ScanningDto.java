package it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto;

public abstract class ScanningDto {

    private int antennaId;
    private long timestamp;
    private String antennaIdDb;

    public int getAntennaId() {
        return antennaId;
    }

    public String getAntennaIdDb() {
        return antennaIdDb;
    }

    public void setAntennaId(int antennaId) {
        this.antennaId = antennaId;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public void setAntennaIdDb(String antennaIdDb) {
        this.antennaIdDb = antennaIdDb;
    }

    /**
     * Return a unique identifier of the tag.
     *
     * @return epc for UHF tags and uid for NFC tags
     */
    public abstract String getCode();
}
