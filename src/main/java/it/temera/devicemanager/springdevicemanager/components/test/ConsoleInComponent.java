package it.temera.devicemanager.springdevicemanager.components.test;

import it.temera.devicemanager.springdevicemanager.DMComponent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Slf4j
@Component
public class ConsoleInComponent extends DMComponent {
    public Boolean killed = false;

    public ConsoleInComponent() {
        log.info("Creating console in component");
        var thread = new Thread(() -> {
            Scanner scanner = new Scanner(System.in);
            while (!killed) {
                var content = scanner.nextLine();
                fire(new TestEvent(content));
            }
        });
        thread.start();
    }

    @Override
    public void destroy() {
        killed = true;
    }

}
