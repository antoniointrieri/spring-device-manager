package it.temera.devicemanager.springdevicemanager.components.mhart.fsm;

import it.temera.devicemanager.springdevicemanager.fsm.FSMMessage;
import lombok.Getter;

@Getter
public class MhartMessage implements FSMMessage<MhartMessageType> {
    private final MhartMessageType type;
    private final String params;

    public MhartMessage(MhartMessageType type, String params) {
        this.type = type;
        this.params = params;
    }

    public MhartMessage(MhartMessageType type) {
        this(type, null);
    }
}
