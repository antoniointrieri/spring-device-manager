package it.temera.devicemanager.springdevicemanager.components.drivers.impjnj;

import com.impinj.octane.*;
import it.temera.devicemanager.springdevicemanager.components.drivers.ReaderManagerComponent;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.AbstractRfidReaderDriver;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidAntennaDto;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidCommandDto;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidResponseDto;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.UhfScanningDto;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.events.*;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.exceptions.RfidReaderException;
import it.temera.devicemanager.springdevicemanager.components.drivers.impjnj.dto.ImpinjSpeedwayGpoPortDto;
import it.temera.devicemanager.springdevicemanager.components.drivers.impjnj.dto.ImpinjSpeedwayOctaneDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class ImpinjSpeedwayOctaneDriver extends AbstractRfidReaderDriver<ImpinjSpeedwayOctaneDto>
        implements TagReportListener, KeepaliveListener, ConnectionLostListener,
        ReaderStartListener, ReaderStopListener, GpiChangeListener {

    private final ImpinjReader octaneDriver;

    private final Map<Integer, String> antennaIdsMap = new HashMap<>();

    private CompletableFuture<RfidResponseDto> startFuture; //questa future viene completata quando viene lanciato l'evento start dal lettore
    private CompletableFuture<RfidResponseDto> stopFuture;  //questa future viene completata quando viene lanciato l'evento stop dal lettore

    private ReaderManagerComponent readerManager;

    public ImpinjSpeedwayOctaneDriver(ReaderManagerComponent readerManager) {
        this.readerManager = readerManager;
        octaneDriver = new ImpinjReader();
        octaneDriver.setTagReportListener(this);
        octaneDriver.setKeepaliveListener(this);
        octaneDriver.setConnectionLostListener(this);
        octaneDriver.setReaderStartListener(this);
        octaneDriver.setReaderStopListener(this);
        octaneDriver.setGpiChangeListener(this);
    }

    @Override
    public void connect(ImpinjSpeedwayOctaneDto reader) {
        try {
            if (octaneDriver.isConnected()) {
                log.warn("Reader already connected. Forcing disconnection");
                disconnect();
            }
            octaneDriver.connect(reader.getIpAddress());
        } catch (OctaneSdkException e) {
            throw new RfidReaderException(e);
        }
        log.info(String.format("Impinj Speedway [%s - %s] - Connected", reader.getCode(), reader.getId()));
    }


    @Override
    public void disconnect() {
        octaneDriver.disconnect();
        started = false;
        if (startFuture != null && !startFuture.isDone()) {
            startFuture.cancel(true);
        }
        if (stopFuture != null && !stopFuture.isDone()) {
            stopFuture.cancel(true);
        }
        log.info(String.format("Impinj Speedway [%s - %s] - Disconnected", reader.getCode(), reader.getId()));
    }

    @Override
    public CompletableFuture<RfidResponseDto> start(ImpinjSpeedwayOctaneDto reader) {
        this.reader = reader;
        applySettingsToReader();
        if (!octaneDriver.isConnected()) {
            throw new RfidReaderException("Reader not connected");
        }
        if (started) {
            log.info("Ignoring start request: reader already started");
            return CompletableFuture.completedFuture(new RfidResponseDto(false));
        }
        startFuture = new CompletableFuture<>();
        if (!reader.isTriggerEnabled() && !reader.isTimeoutEnabled()) {
            try {
                octaneDriver.start();
            } catch (OctaneSdkException e) {
                throw new RfidReaderException(e);
            }
        }
        log.info(String.format("Impinj Speedway [%s - %s] - Starting", reader.getCode(), reader.getId()));
        started = true;
        startFuture.completeOnTimeout(new RfidResponseDto(false), 10, TimeUnit.SECONDS); //FIXME parametrizzare il timeout
        return startFuture;
    }

    protected void applySettingsToReader() {
        for (RfidAntennaDto antenna : reader.getRfidAntennas()) {
            antennaIdsMap.put(antenna.getRfidReaderPort(), antenna.getId());
        }
        Settings settings = octaneDriver.queryDefaultSettings();
        ReportConfig report = settings.getReport();
        report.setIncludeAntennaPortNumber(true);
        report.setIncludePeakRssi(true);
        report.setIncludeFirstSeenTime(true);
        report.setIncludeLastSeenTime(true);
        report.setIncludeSeenCount(true);
        report.setIncludePhaseAngle(true);
        report.setIncludeDopplerFrequency(true);

        if (reader.getGpoPortConfigurations() != null) {
            reader.getGpoPortConfigurations().forEach(c -> addGpoPortToSettings(c, settings));
        }

        report.setMode(ReportMode.Individual);

        if (reader.getSession() != null) {
            settings.setSession(reader.getSession());
            log.debug(String.format("Impinj Speedway [%s - %s] - Reader Session [%s]", reader.getCode(),
                    reader.getId(), settings.getSession()));
        }

        if (reader.getSearchMode() != null) {
            SearchMode searchMode = null;
            switch (reader.getSearchMode()) {
                case DUAL_TARGET:
                    searchMode = SearchMode.DualTarget;
                    break;
                case SINGLE_TARGET:
                    searchMode = SearchMode.SingleTarget;
                    break;
                default:
                    break;
            }

            settings.setSearchMode(searchMode);

            log.debug(String.format("Impinj Speedway [%s - %s] - Reader Search Mode [%s]", reader.getCode(),
                    reader.getId(), settings.getSearchMode()));
        }

        if (reader.getReaderMode() != null) {
            ReaderMode mode = null;
            switch (reader.getReaderMode()) {
                case AUTO_SET_DENSE_READER:
                    mode = ReaderMode.AutoSetDenseReader;
                    break;
                case AUTO_SET_DENSE_READER_DEEP_SCAN:
                    mode = ReaderMode.AutoSetDenseReaderDeepScan;
                    break;
                case DENSE_READER_M4:
                    mode = ReaderMode.DenseReaderM4;
                    break;
                case DENSE_READER_M4_TWO:
                    mode = ReaderMode.DenseReaderM4Two;
                    break;
                case DENSE_READER_M8:
                    mode = ReaderMode.DenseReaderM8;
                    break;
                case HYBRID:
                    mode = ReaderMode.Hybrid;
                    break;
                case MAX_MILLER:
                    mode = ReaderMode.MaxMiller;
                    break;
                case MAX_THROUGHPUT:
                    mode = ReaderMode.MaxThroughput;
                    break;
                default:
                    break;
            }

            settings.setReaderMode(mode);
        } else {
            settings.setReaderMode(ReaderMode.AutoSetDenseReader);
        }

        log.debug(String.format("Impinj Speedway [%s - %s] - Reader Reader Mode [%s]", reader.getCode(),
                reader.getId(), settings.getReaderMode()));

        AntennaConfigGroup antennas = settings.getAntennas();
        antennas.disableAll();

        for (RfidAntennaDto antenna : reader.getRfidAntennas()) {
            short readerPort = (short) antenna.getRfidReaderPort();
            try {
                antennas.enableById(new short[]{readerPort});
                var antennaConfig = antennas.getAntenna(readerPort);
                antennaConfig.setIsMaxRxSensitivity(false);
                antennaConfig.setIsMaxTxPower(false);
                antennaConfig.setTxPowerinDbm(antenna.getTxPower());
                antennaConfig.setRxSensitivityinDbm(antenna.getRxSensitivity());
                log.debug(String.format(
                        "Impinj Speedway [%s - %s] - Reader Antenna Configurations: Port[%s] Power(dB)[%s] Sensitivity(dB)[%s]",
                        reader.getCode(), reader.getId(), readerPort,
                        antennaConfig.getTxPowerinDbm(),
                        antennaConfig.getRxSensitivityinDbm()));
            } catch (OctaneSdkException e) {
                throw new RfidReaderException(e);
            }
        }

        var keepalives = settings.getKeepalives();
        keepalives.setEnabled(false);
        if (reader.isKeepAliveEnabled()) {
            keepalives.setEnabled(true);
            keepalives.setPeriodInMs(reader.getKeepAlivePeriod());
            log.debug(String.format("Impinj Speedway [%s - %s] - Reader Keep Alive Enabled with period [%s]",
                    reader.getCode(), reader.getId(), keepalives.getPeriodInMs()));
        }

        keepalives.setEnableLinkMonitorMode(false);
        if (reader.isLinkMonitorModeEnabled()) {
            keepalives.setEnableLinkMonitorMode(true);
            keepalives.setLinkDownThreshold((int) reader.getLinkDownThreshold());
            log.debug(String.format("Impinj Speedway [%s - %s] - Reader Link Monitor Mode Enabled with period [%s]",
                    reader.getCode(), reader.getId(), keepalives.getLinkDownThreshold()));
        }

        if (reader.isTriggerEnabled()) {
            var gpiStartConfig = settings.getGpis().get(reader.getGpioStartPort() - 1);
            gpiStartConfig.setIsEnabled(true);
            gpiStartConfig.setDebounceInMs(reader.getDebounceTime());
            log.debug(String.format(
                    "Impinj Speedway [%s - %s] - GPIO Port (%d) CONF: Port[%d]  Enabled[%b]    DebounceMs[%d]",
                    reader.getCode(), reader.getId(), reader.getGpioStartPort() - 1,
                    gpiStartConfig.getPortNumber(),
                    gpiStartConfig.getIsEnabled(),
                    gpiStartConfig.getDebounceInMs()));

            var gpiStopConfig = settings.getGpis().get(reader.getGpioStopPort() - 1);
            gpiStopConfig.setIsEnabled(true);
            gpiStopConfig.setDebounceInMs(reader.getDebounceTime());

            log.debug(String.format(
                    "Impinj Speedway [%s - %s] - GPIO Port (%d) CONF: Port[%d]  Enabled[%b]    DebounceMs[%d]",
                    reader.getCode(), reader.getId(), reader.getGpioStopPort() - 1,
                    gpiStopConfig.getPortNumber(),
                    gpiStopConfig.getIsEnabled(),
                    gpiStopConfig.getDebounceInMs()));

            var autostart = settings.getAutoStart();
            autostart.setGpiPortNumber(reader.getGpioStartPort());
            autostart.setMode(AutoStartMode.GpiTrigger);
            autostart.setGpiLevel(reader.getStartType().getOctaneValue());

            log.debug(String.format(
                    "Impinj Speedway [%s - %s] - AUTOSTART CONF: Port number[%d]   First Delay ms[%d]  GPILevel[%b]  Mode[%d]   PeridodMs[%d]",
                    reader.getCode(), reader.getId(), autostart.getGpiPortNumber(),
                    autostart.getFirstDelayInMs(), autostart.getGpiLevel(),
                    autostart.getMode().getValue(), autostart.getPeriodInMs()));

            var autostop = settings.getAutoStop();
            autostop.setMode(AutoStopMode.GpiTrigger);
            autostop.setGpiPortNumber(reader.getGpioStopPort());
            autostop.setGpiLevel(reader.getStopType().getOctaneValue());

            log.info(String.format(
                    "Impinj Speedway [%s - %s] - AUTOSTOP CONF: Port number[%d]   Duration ms[%d]  GPILevel[%b]  Mode[%d]   TimeoutMs[%d]",
                    reader.getCode(), reader.getId(), autostop.getGpiPortNumber(),
                    autostop.getDurationInMs(), autostop.getGpiLevel(),
                    autostop.getMode().getValue(), autostop.getTimeout()));

        }

        if (reader.isTimeoutEnabled()) {
            var gpiStartConfig = settings.getGpis().get(reader.getGpioStartPort() - 1);
            gpiStartConfig.setIsEnabled(true);
            gpiStartConfig.setDebounceInMs(reader.getDebounceTime());
            log.info(String.format(
                    "Impinj Speedway [%s - %s] - GPIO Port (%d) CONF: Port[%d]  Enabled[%b]    DebounceMs[%d]",
                    reader.getCode(), reader.getId(), reader.getGpioStartPort() - 1,
                    gpiStartConfig.getPortNumber(),
                    gpiStartConfig.getIsEnabled(),
                    gpiStartConfig.getDebounceInMs()));

            // set autostart to go on GPI level
            settings.getAutoStart().setGpiPortNumber(reader.getGpioStartPort());
            settings.getAutoStart().setMode(AutoStartMode.GpiTrigger);
            settings.getAutoStart().setGpiLevel(reader.getStartType().getOctaneValue());

            log.info(String.format(
                    "Impinj Speedway [%s - %s] - AUTOSTART CONF: Port number[%d]   First Delay ms[%d]  GPILevel[%b]  Mode[%d]   PeridodMs[%d]",
                    reader.getCode(), reader.getId(), settings.getAutoStart().getGpiPortNumber(),
                    settings.getAutoStart().getFirstDelayInMs(), settings.getAutoStart().getGpiLevel(),
                    settings.getAutoStart().getMode().getValue(), settings.getAutoStart().getPeriodInMs()));

            settings.getAutoStop().setMode(AutoStopMode.Duration);
            settings.getAutoStop().setDurationInMs(reader.getTimeout());
            log.info(String.format("Impinj Speedway [%s - %s] - AUTOSTOP CONF: Mode[%d]   Duration ms[%d]",
                    reader.getCode(), reader.getId(), settings.getAutoStop().getMode().getValue(),
                    settings.getAutoStop().getDurationInMs()));
        }

        try {
            octaneDriver.applySettings(settings);
        } catch (OctaneSdkException e) {
            throw new RfidReaderException(e);
        }
    }

    @Override
    public CompletableFuture<RfidResponseDto> stop() {
        if (!started) {
            log.info("Ignoring stop request: reader already stopped");
            return CompletableFuture.completedFuture(new RfidResponseDto(false));
        }
        try {
            stopFuture = new CompletableFuture<>();
            octaneDriver.stop();
        } catch (OctaneSdkException e) {
            throw new RfidReaderException(e);
        }
        log.info(String.format("Impinj Speedway [%s - %s] - Stopped", reader.getCode(), reader.getId()));
        started = false;
        stopFuture.completeOnTimeout(new RfidResponseDto(false), 10, TimeUnit.SECONDS); //FIXME parametrizzare il timeout
        return stopFuture;
    }

    @Override
    public CompletableFuture<RfidResponseDto> write(RfidCommandDto dto) {
        throw new UnsupportedOperationException("Write operation not implemented");
    }

    @Override
    public CompletableFuture<RfidResponseDto> read(RfidCommandDto dto) {
        if (!octaneDriver.isConnected()) {
            throw new RfidReaderException("Reader not connected");
        }
        ImpinjSpeedwayOctaneOperationListener listener;

        try {
            // Get the default settings
            Settings settings = octaneDriver.queryDefaultSettings();

            settings.getAntennas().enableAll();
            settings.getAntennas().setIsMaxTxPower(true);

            // set session one so we see the tag only once every few seconds
            settings.getReport().setIncludeAntennaPortNumber(true);
            settings.setReaderMode(ReaderMode.AutoSetDenseReader);
            settings.setSearchMode(SearchMode.SingleTarget);
            settings.setSession(1);
            // turn these on so we have them always
            settings.getReport().setIncludePcBits(true);

            // Set periodic mode so we reset the tag and it shows up with its
            // new EPC
            settings.getAutoStart().setMode(AutoStartMode.Periodic);
            settings.getAutoStart().setPeriodInMs(2000);
            settings.getAutoStop().setMode(AutoStopMode.Duration);
            settings.getAutoStop().setDurationInMs(1000);

            log.debug(String.format(
                    "Impinj Speedway [%s - %s] - Rfid Reader Configurations: All antennas enabled at max power \\n ReaderMode [%s] \\n Search Mode [%s] \\n Session [%s]",
                    reader.getCode(), reader.getId(), settings.getReaderMode(), settings.getSearchMode(),
                    settings.getSession()));

            // Apply the new settings
            octaneDriver.applySettings(settings);

            TagOpSequence seq = new TagOpSequence();
            seq.setOps(new ArrayList<>());
            seq.setExecutionCount((short) 1); // delete after one time
            seq.setState(SequenceState.Active);
            seq.setId(1);

            seq.setTargetTag(new TargetTag());
            seq.getTargetTag().setBitPointer(BitPointers.Epc);
            seq.getTargetTag().setMemoryBank(com.impinj.octane.MemoryBank.Epc);
            seq.getTargetTag().setData(dto.getEpc());

            TagReadOp operation = new TagReadOp();
            operation.Id = 1;
            switch (dto.getMemoryBank()) {
                case EPC:
                    operation.setMemoryBank(com.impinj.octane.MemoryBank.Epc);
                    break;
                case TID:
                    operation.setMemoryBank(com.impinj.octane.MemoryBank.Tid);
                    break;
                case USER:
                    operation.setMemoryBank(com.impinj.octane.MemoryBank.User);
                    break;
                case RESERVED:
                    operation.setMemoryBank(com.impinj.octane.MemoryBank.Reserved);
                    break;
            }

            operation.setWordPointer((short) (dto.getAddress() == null ? 0 : dto.getAddress()));
            operation.setWordCount((short) (dto.getNumBlock() == null ? 0 : dto.getNumBlock()));

            log.debug(String.format(
                    "Impinj Speedway [%s - %s] - Read Operation Configurations: \\n Memory Bank [%s] \\n Word Count [%s] \\n WordPointer [%s]",
                    reader.getCode(), reader.getId(), operation.getMemoryBank(), operation.getWordCount(),
                    operation.getWordPointer()));

            // add to the list
            seq.getOps().add(operation);

            octaneDriver.addOpSequence(seq);
            var future = new CompletableFuture<RfidResponseDto>();

            listener = new ImpinjSpeedwayOctaneOperationListener(future);
            octaneDriver.setTagOpCompleteListener(listener);
            octaneDriver.start(); //la stop/disconnect è demandata al listener

            return future;
        } catch (Exception e) {
            log.error(e.getMessage());
            stop();
            throw new RfidReaderException(e);
        }
    }

    @Override
    public CompletableFuture<RfidResponseDto> kill(RfidCommandDto dto) {
        throw new UnsupportedOperationException("Kill operation not implemented");
    }


    @Override
    public void onTagReported(ImpinjReader impinjReader, TagReport report) {
        List<Tag> tags = report.getTags();
        for (Tag t : tags) {
            UhfScanningDto scanning = parseScanning(t);
            log.debug(String.format("Scanned epc [%s] on antenna [%s]", scanning.getEpc(), scanning.getAntennaIdDb()));
            readerManager.fire(new TagReadEvent<>(scanning, reader));
        }
    }

    private UhfScanningDto parseScanning(Tag t) {
        UhfScanningDto scanning = new UhfScanningDto();
        scanning.setEpc(normalizeEpc(t.getEpc().toString()));

        if (t.isAntennaPortNumberPresent()) {
            scanning.setAntennaId(t.getAntennaPortNumber());
            scanning.setAntennaIdDb(antennaIdsMap.get((int) t.getAntennaPortNumber()));
        }

        if (t.isFirstSeenTimePresent()) {
            scanning.setFirstSeen(t.getFirstSeenTime().getLocalDateTime().getTime());
        }

        if (t.isLastSeenTimePresent()) {
            scanning.setLastSeen(t.getLastSeenTime().getLocalDateTime().getTime());
        }

        if (t.isSeenCountPresent()) {
            scanning.setTagSeenCount(t.getTagSeenCount());
        }

        if (t.isPeakRssiInDbmPresent()) {
            scanning.setPeakRSSI(t.getPeakRssiInDbm());
        }

        if (t.isRfPhaseAnglePresent()) {
            scanning.setPhaseAngleInRadians(t.getPhaseAngleInRadians());
        }

        if (t.isRfDopplerFrequencyPresent()) {
            scanning.setRfDopplerFrequency(t.getRfDopplerFrequency());
        }
        return scanning;
    }

    @Override
    public void onReaderStart(ImpinjReader impinjReader, ReaderStartEvent event) {
        readerManager.fire(new RfidReaderStartEvent<>(reader));
        startFuture.complete(new RfidResponseDto(true));
    }

    @Override
    public void onReaderStop(ImpinjReader impinjReader, ReaderStopEvent event) {
        readerManager.fire(new RfidReaderStopEvent<>(reader));
        stopFuture.complete(new RfidResponseDto(true));
    }

    @Override
    public void onConnectionLost(ImpinjReader impinjReader) {
        readerManager.fire(new RfidReaderStopEvent<>(reader));
    }

    @Override
    public void onKeepalive(ImpinjReader impinjReader, KeepaliveEvent event) {
        updateKeepalive();
    }


    @Override
    public void onGpiChanged(ImpinjReader impinjReader, GpiEvent event) {
        readerManager.fire(new RfidReaderGPIEvent<>(event.isState(), reader));
    }

    @Override
    public boolean isKeepAliveEnabled() {
        return reader.isKeepAliveEnabled();
    }

    private void addGpoPortToSettings(ImpinjSpeedwayGpoPortDto portDto, Settings impinjSettings) {
        log.info(String.format("Apply GPO configuration for reader [%s], port [%s], mode [%s], pulse duration [%s]",
                reader.getId(), portDto.getPortNumber(), portDto.getGpoMode(), portDto.getPulseDuration()));
        var gpo = impinjSettings.getGpos().get(portDto.getPortNumber() - 1);
        gpo.setMode(portDto.getGpoMode().getImpinjGpoMode());
        gpo.setGpoPulseDurationMsec(portDto.getPulseDuration());
    }


}
