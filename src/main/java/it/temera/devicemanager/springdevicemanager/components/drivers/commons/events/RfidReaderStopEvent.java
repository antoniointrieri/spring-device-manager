package it.temera.devicemanager.springdevicemanager.components.drivers.commons.events;

import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidReaderDto;

public class RfidReaderStopEvent<R extends RfidReaderDto> extends RfidReaderEvent<R> {
    public RfidReaderStopEvent(R reader) {
        super(reader);
    }
}
