package it.temera.devicemanager.springdevicemanager.components.drivers.commons.events;

import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidReaderDto;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.ScanningDto;
import lombok.Getter;

@Getter
public class TagReadEvent<T extends ScanningDto, R extends RfidReaderDto> extends RfidReaderEvent<R> {
    private final T scanning;

    public TagReadEvent(T scanning, R reader) {
        super(reader);
        this.scanning = scanning;
    }

}
