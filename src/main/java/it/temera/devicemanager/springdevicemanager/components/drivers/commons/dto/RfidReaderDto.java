package it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import it.temera.devicemanager.springdevicemanager.components.drivers.impjnj.dto.ImpinjSpeedwayOctaneDto;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ImpinjSpeedwayOctaneDto.class)
})
public abstract class RfidReaderDto {

    private String id;
    private String code;
    private String ipAddress;
    private List<RfidAntennaDto> rfidAntennas = new ArrayList<>();
    private long maxConnectionTimeout;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public List<RfidAntennaDto> getRfidAntennas() {
        return rfidAntennas;
    }

    public void setRfidAntennas(List<RfidAntennaDto> rfidAntennas) {
        this.rfidAntennas = rfidAntennas;
    }

    public long getMaxConnectionTimeout() {
        return maxConnectionTimeout;
    }

    public void setMaxConnectionTimeout(long maxConnectionTimeout) {
        this.maxConnectionTimeout = maxConnectionTimeout;
    }

}
