package it.temera.devicemanager.springdevicemanager.components.mhart.exceptions;

public class MhartDriverException extends RuntimeException {
    private String alarmCode;
    public MhartDriverException(String message) {
        super(message);
    }

    public MhartDriverException(String message, String alarmCode) {
        super(message);
        this.alarmCode = alarmCode;
    }

    public String getAlarmCode() {
        return alarmCode;
    }
}
