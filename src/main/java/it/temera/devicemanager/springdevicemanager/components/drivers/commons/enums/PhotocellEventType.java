package it.temera.devicemanager.springdevicemanager.components.drivers.commons.enums;

/**
 * @author Dario Veneziani
 * @since 4.0
 *
 */
/*
 * 0 FOTOCELLULA APERTA
 * 1 FOTOCELLULA CHIUSA
 *
 * FALLING 0 --> 1
 * RISING  1 --> 0
 */
public enum PhotocellEventType {
    FALLING(false), RISING(true);

    private boolean octaneValue;

    PhotocellEventType(boolean octaneValue) {
        this.octaneValue = octaneValue;
    }

    @Deprecated
    /**
     * @deprecated <p> Use {@code getBoolean} instead</p>
     */
    public boolean getOctaneValue() {
        return octaneValue;
    }

    public boolean getBoolean() {
        return octaneValue;
    }

}
