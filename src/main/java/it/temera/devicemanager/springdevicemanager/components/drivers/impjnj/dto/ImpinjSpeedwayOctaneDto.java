package it.temera.devicemanager.springdevicemanager.components.drivers.impjnj.dto;

import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidReaderDto;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.enums.PhotocellEventType;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.enums.RfidReaderMode;
import it.temera.devicemanager.springdevicemanager.components.drivers.commons.enums.RfidReaderSearchMode;

import java.util.List;

public class ImpinjSpeedwayOctaneDto extends RfidReaderDto {

    private boolean keepAliveEnabled;

    private long keepAlivePeriod;

    private boolean linkMonitorModeEnabled;

    private long linkDownThreshold;

    /**
     * Abilita la funzionalità di accensione/spegnimento delle antenne
     * attraverso le porte del gpio
     */
    private boolean triggerEnabled;

    /**
     * Porta su cui si sta in ascolto per l'accnsione delle antenne
     */
    private int gpioStartPort;

    /**
     * L'evento su cui si esegue l'operazione di start delle antenne
     */
    private PhotocellEventType startType;

    /**
     * Porta su cui si sta in ascolto per l'accnsione delle antenne
     */
    private int gpioStopPort;

    /**
     * L'evento su cui si esegue l'operazione di start delle antenne
     */
    private PhotocellEventType stopType;

    /**
     * Il tempo minimo che deve intercorrere tra due eventi su una porta del
     * gpio in maniera che vengano considerati validi
     */
    private long debounceTime;

    private Integer session;

    private RfidReaderSearchMode searchMode;

    private RfidReaderMode readerMode;

    private List<ImpinjSpeedwayGpoPortDto> gpoPortConfigurations;

    private boolean timeoutEnabled;

    private long timeout;

    public boolean isKeepAliveEnabled() {
        return keepAliveEnabled;
    }

    public void setKeepAliveEnabled(boolean keepAliveEnabled) {
        this.keepAliveEnabled = keepAliveEnabled;
    }

    public long getKeepAlivePeriod() {
        return keepAlivePeriod;
    }

    public void setKeepAlivePeriod(long keepAlivePeriod) {
        this.keepAlivePeriod = keepAlivePeriod;
    }

    public boolean isLinkMonitorModeEnabled() {
        return linkMonitorModeEnabled;
    }

    public void setLinkMonitorModeEnabled(boolean linkMonitorModeEnabled) {
        this.linkMonitorModeEnabled = linkMonitorModeEnabled;
    }

    public long getLinkDownThreshold() {
        return linkDownThreshold;
    }

    public void setLinkDownThreshold(long linkDownThreshold) {
        this.linkDownThreshold = linkDownThreshold;
    }

    public boolean isTriggerEnabled() {
        return triggerEnabled;
    }

    public void setTriggerEnabled(boolean triggerEnabled) {
        this.triggerEnabled = triggerEnabled;
    }

    public int getGpioStartPort() {
        return gpioStartPort;
    }

    public void setGpioStartPort(int gpioStartPort) {
        this.gpioStartPort = gpioStartPort;
    }

    public PhotocellEventType getStartType() {
        return startType;
    }

    public void setStartType(PhotocellEventType startType) {
        this.startType = startType;
    }

    public int getGpioStopPort() {
        return gpioStopPort;
    }

    public void setGpioStopPort(int gpioStopPort) {
        this.gpioStopPort = gpioStopPort;
    }

    public PhotocellEventType getStopType() {
        return stopType;
    }

    public void setStopType(PhotocellEventType stopType) {
        this.stopType = stopType;
    }

    public long getDebounceTime() {
        return debounceTime;
    }

    public void setDebounceTime(long debounceTime) {
        this.debounceTime = debounceTime;
    }

    public Integer getSession() {
        return session;
    }

    public void setSession(Integer session) {
        this.session = session;
    }

    public RfidReaderSearchMode getSearchMode() {
        return searchMode;
    }

    public void setSearchMode(RfidReaderSearchMode searchMode) {
        this.searchMode = searchMode;
    }

    public RfidReaderMode getReaderMode() {
        return readerMode;
    }

    public void setReaderMode(RfidReaderMode readerMode) {
        this.readerMode = readerMode;
    }

    public List<ImpinjSpeedwayGpoPortDto> getGpoPortConfigurations() {
        return gpoPortConfigurations;
    }

    public void setGpoPortConfigurations(List<ImpinjSpeedwayGpoPortDto> gpoPortConfigurations) {
        this.gpoPortConfigurations = gpoPortConfigurations;
    }

    public boolean isTimeoutEnabled() {
        return timeoutEnabled;
    }

    public void setTimeoutEnabled(boolean timeoutEnabled) {
        this.timeoutEnabled = timeoutEnabled;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

}
