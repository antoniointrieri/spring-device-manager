package it.temera.devicemanager.springdevicemanager.components.drivers.commons.enums;

/**
 * @author Dario Veneziani
 * @since 4.0
 *
 */
public enum RfidReaderMode {
    //@formatter:off
    AUTO_SET_DENSE_READER,
    AUTO_SET_DENSE_READER_DEEP_SCAN,
    DENSE_READER_M4,
    DENSE_READER_M4_TWO,
    DENSE_READER_M8,
    HYBRID,
    MAX_MILLER,
    MAX_THROUGHPUT
    //@formatter:on
}
