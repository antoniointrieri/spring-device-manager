package it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto;

public class RfidAntennaDto {

    private String id;
    private String code;
    private int rfidReaderPort;
    private float txPower;
    private float rxSensitivity;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getRfidReaderPort() {
        return rfidReaderPort;
    }

    public void setRfidReaderPort(int rfidReaderPort) {
        this.rfidReaderPort = rfidReaderPort;
    }

    public float getTxPower() {
        return txPower;
    }

    public void setTxPower(float txPower) {
        this.txPower = txPower;
    }

    public float getRxSensitivity() {
        return rxSensitivity;
    }

    public void setRxSensitivity(float rxSensitivity) {
        this.rxSensitivity = rxSensitivity;
    }

}
