package it.temera.devicemanager.springdevicemanager.components.mhart.fsm;

import it.temera.devicemanager.springdevicemanager.fsm.FSMMessageType;

public enum MhartMessageType implements FSMMessageType {
    PLC_READY,
    CLIENT_READY,
    PLC_DESTINATION_REQUEST,
    PLC_CLEAR,
    CLIENT_DESTINATION_RESPONSE,
    CLIENT_ALARM,
    CLIENT_RESET

}
