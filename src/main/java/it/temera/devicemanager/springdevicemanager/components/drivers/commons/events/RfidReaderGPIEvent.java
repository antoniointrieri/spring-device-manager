package it.temera.devicemanager.springdevicemanager.components.drivers.commons.events;

import it.temera.devicemanager.springdevicemanager.components.drivers.commons.dto.RfidReaderDto;
import lombok.Getter;

@Getter
public class RfidReaderGPIEvent<R extends RfidReaderDto> extends RfidReaderEvent<R> {
    private final Boolean state;

    public RfidReaderGPIEvent(Boolean state, R reader) {
        super(reader);
        this.state = state;
    }

    public Boolean getState() {
        return state;
    }
}
